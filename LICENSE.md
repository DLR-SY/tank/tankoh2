<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
SPDX-License-Identifier: MIT
-->


## License

Copyright © 2020 German Aerospace Center (DLR)

This work is licensed under the [MIT](LICENSES/MIT.txt) license.

> **Hint:** We provided the copyright and license information in accordance to the [REUSE Specification 3.0](https://reuse.software/spec/).
