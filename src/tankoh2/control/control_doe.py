# SPDX-FileCopyrightText: 2023 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

"""create DOEs and execute design workflow

Caution:
This module requires fa_pytuils and delismm!
Please contatct the developers for these additional packages.
"""
import csv
import os
from collections import OrderedDict
from datetime import datetime
from multiprocessing import cpu_count

import numpy as np
from delismm.control.tank import getKrigings
from delismm.model.customsystemfunction import AbstractTargetFunction, BoundsHandler
from delismm.model.doe import DOEfromFile, FullFactorialDesign, LatinizedCentroidalVoronoiTesselation
from delismm.model.samplecalculator import getY
from fa_pyutils.service.systemutils import getRunDir

import tankoh2
from tankoh2 import log, programDir
from tankoh2.control.control_winding import createDesign
from tankoh2.control.genericcontrol import resultNamesFrp
from tankoh2.design.existingdesigns import DLightDOE, vphDesign1_isotensoid
from tankoh2.service.exception import Tankoh2Error
from tankoh2.service.plot.doeplot import plotGeometryRange
from tankoh2.service.utilities import indent


class TankWinder(AbstractTargetFunction):
    """"""

    name = "tank winder"

    def __init__(self, lb, ub, runDir, designKwargs):
        """"""
        AbstractTargetFunction.__init__(self, lb, ub, resultNames=resultNamesFrp)
        self.doParallelization = ["local"]
        self.runDir = runDir
        self.allowFailedSample = True
        self.designKwargs = designKwargs
        """keyword arguments defining constants for the tank design"""
        self.asyncMaxProcesses = int(np.ceil(cpu_count() * 2 / 3))

    def _call(self, parameters):
        """call function for the model"""
        runDir = getRunDir(basePath=os.path.join(self.runDir), useMilliSeconds=True)
        paramDict = OrderedDict(zip(self.parameterNames, parameters))
        inputDict = OrderedDict()
        inputDict.update(self.designKwargs)
        inputDict.update(paramDict)
        inputDict["runDir"] = runDir

        result = createDesign(**inputDict)
        return result

    def getNumberOfNewJobs(self):
        return self.asyncMaxProcesses


def getDesignAndBounds(name):
    """returns base design properties (like in existingdesigns) of a tank and upper/lower bounds for the doe

    :param name: name of the design and bounds to return. Not case sensitive!
    :return: designKwargs, lowerBoundDict, upperBoundDict, numberOfSamples
    """
    allowedNames = {"dlight", "exact_cyl_isotensoid", "exact_conical_isotensoid", "vph2"}
    if name not in allowedNames:
        raise Tankoh2Error(f"Parameter name={name} unknown. Allowed names: {allowedNames}")
    name = name.lower()
    numberOfSamples = 201
    units = ["[mm]", "[mm]", "[MPa]"]
    doeClass = LatinizedCentroidalVoronoiTesselation
    if name == "dlight":
        lb = OrderedDict([("dcyl", 100.0), ("lcyl", 800), ("pressure", 50)])  # [mm, mm , MPa]
        ub = OrderedDict([("dcyl", 800.0), ("lcyl", 5000), ("pressure", 200)])
        designKwargs = DLightDOE
    elif name == "exact_cyl_isotensoid":
        lb = OrderedDict([("dcyl", 1000.0), ("lcyl", 150), ("pressure", 0.1)])  # [mm, mm , MPa]
        ub = OrderedDict([("dcyl", 4000.0), ("lcyl", 3000), ("pressure", 1)])
        designKwargs = vphDesign1_isotensoid.copy()
        designKwargs["targetFuncWeights"] = [1.0, 0.2, 0.0, 0.0, 0, 0]
        designKwargs["verbosePlot"] = True
        designKwargs["numberOfRovings"] = 12
        designKwargs.pop("lcyl")
        designKwargs.pop("safetyFactor")
        if 0:  # for testing
            numberOfSamples = 3
            designKwargs["maxLayers"] = 3
    elif name == "exact_conical_isotensoid":
        units = ["[mm]", "[mm]", "[MPa]", "[-]", "[-]"]
        lb = OrderedDict([("dcyl", 1000.0), ("lcyl", 150), ("pressure", 0.1), ("alpha", 0.2), ("beta", 0.5)])
        ub = OrderedDict([("dcyl", 4000.0), ("lcyl", 3000), ("pressure", 1), ("alpha", 0.8), ("beta", 2)])
        designKwargs = vphDesign1_isotensoid.copy()
        designKwargs.pop("safetyFactor")
        addArgs = OrderedDict(
            [
                ("targetFuncWeights", [1.0, 0.2, 1.0, 0.0, 0, 0]),
                ("verbosePlot", True),
                ("numberOfRovings", 12),
                ("gamma", 0.3),
                ("domeType", "conicalIsotensoid"),
                ("dome2Type", "isotensoid"),
                ("nodeNumber", 1000),
            ]
        )
        designKwargs.update(addArgs)
    elif name == "vph2":
        lb = OrderedDict([("minPressure", 0.0), ("safetyFactor", 1)])  # [MPa, -]
        ub = OrderedDict([("minPressure", 0.18), ("safetyFactor", 2.5)])
        designKwargs = {"configFile": "vph2_smr_iff_2bar_param_study"}
        doeClass = FullFactorialDesign
        sampleCount1d = 5
        numberOfSamples = sampleCount1d**2
    return designKwargs, lb, ub, numberOfSamples, doeClass, units


def collectYResults(runDir):

    results = list()
    results_path = runDir
    all_folders = [name for name in os.listdir(results_path) if os.path.isdir(os.path.join(results_path, name))]

    for count, filePath in enumerate(all_folders):
        worker_results_path = results_path + "/" + filePath
        all_sub_folders = [
            name for name in os.listdir(worker_results_path) if os.path.isdir(os.path.join(worker_results_path, name))
        ]
        for count, filePath in enumerate(all_sub_folders):

            with open(worker_results_path + "/" + filePath + "/all_parameters_and_results.txt", "r") as file:
                reader = csv.reader(file, delimiter="|")
                txt_reader = [x for x in reader]

            variables = [
                "shellMass",
                "linerMass",
                "insulationMass",
                "fairingMass",
                "totalMass",
                "volume",
                "area",
                "lengthAxial",
                "numberOfLayers",
                "cylinderThickness",
                "maxThickness",
                "reserveFactor",
                "gravimetricIndex",
                "stressRatio",
                "hoopHelicalRatio",
                "iterations",
            ]

            run_results = []
            for row in txt_reader:
                try:
                    entry = row[0].replace(" ", "")
                    if entry in variables:
                        run_results.append(float(row[2].replace(" ", "")))
                except (IndexError, ValueError):
                    continue

            results.append(run_results)
    return results


def mainControl(name, sampleXFile, sampleYFolder=""):
    """

    :param name: name of the design and bounds to return. Not case sensitive!
    :param sampleXFile: path and filename to a list with sampleX vaules
    """
    startTime = datetime.now()

    designKwargs, lb, ub, numberOfSamples, doeClass, _ = getDesignAndBounds(name)

    names = list(lb.keys())
    runDir = getRunDir(f"doe_{name}", basePath=os.path.join(programDir, "tmp"))

    winder = TankWinder(lb, ub, runDir, designKwargs)
    if sampleXFile:
        doe = DOEfromFile(sampleXFile)
    else:
        doe = doeClass(numberOfSamples, len(names))

    sampleX = BoundsHandler.scaleToBoundsStatic(doe.sampleXNormalized, list(lb.values()), list(ub.values()))
    doe.xToFile(os.path.join(runDir, "sampleX.txt"))
    doe.xToFileStatic(os.path.join(runDir, "sampleX_bounds.txt"), sampleX)
    if sampleYFolder:
        sampleY = collectYResults(sampleYFolder)
    else:
        sampleY = getY(sampleX, winder, verbose=True, runDir=runDir)

    # store samples
    doe.yToFile(os.path.join(runDir, "sampleY.txt"), winder, sampleY)
    # lcvt.xyToFile(os.path.join(runDir, 'full_doe2.txt'), winder, sampleY, True)

    allSamples = [names + winder.resultNames]
    for inputSample, outputSample in zip(sampleX.T, sampleY):
        if hasattr(outputSample, "__iter__"):
            allSamples.append(list(inputSample) + list(outputSample))
        else:
            allSamples.append(list(inputSample) + list([outputSample]))
    with open(os.path.join(runDir, "full_doe.txt"), "w") as f:
        f.write(indent(allSamples, hasHeader=True))

    duration = datetime.now() - startTime
    log.info(f"runtime {duration.seconds} seconds")


def main():
    createDoe = True
    plotDoe = False
    createSurrogate = False
    mmRunDir = getRunDir("tank_surrogates", basePath=os.path.join(tankoh2.programDir, "tmp"))
    resultNamesIndexesLog10 = [
        ("totalMass[kg]", 4, True),
        ("volume[dm^3]", 5, True),
        ("area[m^2]", 6, False),
        ("lengthAxial[mm]", 7, False),
        ("gravimetricIndex[-]", 10, False),
    ]
    if 0:
        designName = "exact_cyl_isotensoid"
        sampleXFile = "" + r"C:\PycharmProjects\tankoh2\tmp\doe_exact_cyl_isotensoid_20230106_230150/sampleX.txt"
        surrogateDir = "" + r"C:\PycharmProjects\tankoh2\tmp\tank_surrogates_20230109_180336"
    elif 0:
        designName = "exact_conical_isotensoid"
        sampleXFile = "" + r"C:\PycharmProjects\tankoh2\tmp\doe_exact_conical_isotensoid_20230111_180256/sampleX.txt"
        surrogateDir = ""  # + r'C:\PycharmProjects\tankoh2\tmp\tank_surrogates_20230109_180336'
    elif 0:
        designName = "vph2"
        sampleXFile = ""
    else:
        designName = "dlight"
        sampleXFile = ""

    designKwargs, lb, ub, numberOfSamples, doeClass, units = getDesignAndBounds(designName)
    if createDoe:
        mainControl(designName, sampleXFile)
    if plotDoe:
        doe = DOEfromFile(sampleXFile) if sampleXFile else None
        sampleX = BoundsHandler(lb, ub).scaleToBounds(doe.sampleXNormalized)
        plotGeometryRange(lb, ub, show=True, samples=sampleX, addBox=("exact" in designName))
    if createSurrogate:
        parameterNames = lb.keys(lb)
        parameterNames = [name + unit for name, unit in zip(parameterNames, units)]
        krigings = getKrigings(surrogateDir, mmRunDir, parameterNames, resultNamesIndexesLog10)


if __name__ == "__main__":
    main()
