# SPDX-FileCopyrightText: 2023 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

"""
h2 tank optimization
"""

import logging
import sys

from tankoh2.__about__ import __author__, __description__, __programDir__, __title__, __version__
from tankoh2.settings import applySettings

# main program information
name = __title__
programDir = __programDir__
version = __version__
description = __description__
author = __author__


# create logger
level = logging.INFO
formatter = logging.Formatter("%(levelname)s\t%(asctime)s: %(message)s")
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(level)
handler.formatter = formatter
log = logging.getLogger(f"{name}_logger")
log.handlers.append(handler)
log.setLevel(level)

# make mycropychain available
pychainIsLoaded = False
pychain = applySettings()
if pychain is None:  # no error during import and checks
    try:
        # v <= 0.90
        import mycropychain as pychain

        pychainIsLoaded = True
    except ModuleNotFoundError:
        try:
            # v > 0.90
            if sys.version_info.minor == 6:
                import mycropychain36 as pychain

                pychainIsLoaded = True
            elif sys.version_info.minor == 10:
                import mycropychain310 as pychain

                pychainIsLoaded = True
            else:  # sys.version_info.minor == 8
                import mycropychain38 as pychain

                pychainIsLoaded = True
        except ModuleNotFoundError:
            log.error("Could not load mycropychain package")
