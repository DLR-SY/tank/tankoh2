# SPDX-FileCopyrightText: 2023 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

"""very rough estimations of liner and insulation mass"""

from tankoh2.masses.massdata import fairingDens, insulationDens, linerDens
from tankoh2.service.exception import Tankoh2Error
from tankoh2.settings import epsilon


def getAuxMaterials(linerMaterial=None, insulationMaterial=None, fairingMaterial=None):
    """
    :param linerMaterial: name of the liner material to be used
    :param linerMaterial: name of the insulation material to be used
    :param linerMaterial: name of the fairing material to be used
    """

    if linerMaterial not in linerDens:
        raise Tankoh2Error(f"Liner Material {linerMaterial} not found.")
    elif insulationMaterial not in insulationDens:
        raise Tankoh2Error(f"Insulation Material {insulationMaterial} not found.")
    elif fairingMaterial not in fairingDens:
        raise Tankoh2Error(f"Fairing Material {fairingMaterial} not found.")
    else:
        return linerMaterial, insulationMaterial, fairingMaterial


def getLinerMass(liner, linerMatName=None, linerThickness=0.5):
    """
    :param liner: object of type tankoh2.geometry.liner.Liner
    :param linerMatName: name of liner material in linerDens
    :param linerThickness: thickness of the liner [mm]
    """
    if linerThickness < epsilon:
        return 0
    if linerMatName in linerDens:
        rho = linerDens[linerMatName]  # [kg/m**3]
    else:
        rho = next(iter(linerDens.values()))

    volume = liner.getWallVolume(-1 * linerThickness) / 1000 / 1000 / 1000
    return abs(rho * volume)


def getInsulationMass(liner, insulationMatName=None, insulationThickness=127):  # source Brewer fig 3-6
    """
    :param liner: object of type tankoh2.geometry.liner.Liner
    :param insulationMatName: name of insulation material in linerDens
    :param insulationThickness: thickness of the insulation [mm]
    """
    if insulationThickness < epsilon:
        return 0
    if insulationMatName in linerDens:
        rho = insulationDens[insulationMatName]  # [kg/m**3]
    else:
        rho = next(iter(insulationDens.values()))

    volume = liner.getWallVolume(insulationThickness) / 1000 / 1000 / 1000
    return rho * volume


def getFairingMass(liner, fairingMatName=None, fairingThickness=0.5, insulationCfrpThickness=127):
    """
    :param liner: object of type tankoh2.geometry.liner.Liner
    :param fairingMatName: name of fairing material in linerDens
    :param fairingThickness: thickness of the fairing [mm]
    :param insulationCfrpThickness: thickness of the cfrp or metal structure and insulation [mm]
    """
    if fairingThickness < epsilon:
        return 0
    if fairingMatName in linerDens:
        rho = fairingDens[fairingMatName]  # [kg/m**3]
    else:
        rho = next(iter(fairingDens.values()))

    liner = liner.getLinerResizedByThickness(insulationCfrpThickness)
    volume = liner.getWallVolume(fairingThickness) / 1000 / 1000 / 1000
    return rho * volume
