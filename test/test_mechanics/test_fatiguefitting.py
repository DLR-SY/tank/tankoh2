from io import StringIO
from os.path import join

import numpy as np

from tankoh2 import programDir
from tankoh2.mechanics.fatigue import addMeanstressesAndAmplitudesNorm, getFatigueStressFactor
from tankoh2.mechanics.fatiguefitting import (
    FatigueParamFitter,
    fitFatigueParameters,
    fitMastercurveParameters,
    getMeanstressesAndAmplitudesNormWoehler,
    getWoehlerParameters,
    readFatigueTestData,
    readFatigueTestDataTxtFiles,
)
from tankoh2.mechanics.material import FrpMaterialProperties

testDataFrame = readFatigueTestData(join(programDir, "test/test_mechanics/fatigue_test_data/testData.csv"))

# direction, stressRation to given A and B vales
# 11 | -1.0
# 11 | 0.1
# 11 | 10.0
# 12 | -1.0
# 12 | 0.1
# 22 | 0.1
# 22 | 10.0
# 22 | -3.96
R_ref = [-1.0, 0.1, 10.0, -1.0, 0.1, 0.1, 10.0, -3.96]
A_ref = [740.23, 2435.91, 683.31, 75.87, 66.09, 29.66, 143.10, 145.24]  # adjusted, copy-paste error in diss
B_ref = [-0.026, -0.016, -0.008, -0.109, -0.031, -0.028, -0.027, -0.074]
meanStressNormRef = [
    0.39831928658506155,
    -1.9306334322900827,
    -1.07238385991901,
    0.37344902972797517,
    -1.8142612404784058,
    -0.9043785827351171,
    0.35013162179628315,
    -1.7049035791314742,
    -0.7626938929981186,
    0.32827010601953643,
    -1.6021376355749288,
    -0.6432062694998605,
    0.30777358,
    -1.50556608,
    -0.5424382,
]
stressAmplitudeNormRef = [
    0.32589759811505037,
    1.579609171873704,
    1.7969675490534764,
    0.3055492061410706,
    1.484395560391423,
    1.515445192691277,
    0.2864713269242316,
    1.3949211101984789,
    1.2780276044833339,
    0.2685846321978025,
    1.3108398836522146,
    1.0778051002430096,
    0.2518147467928211,
    1.2318267950858237,
    0.9089505031305389,
]
Cs_Ref = [0.028560893436064654]
Ds_Ref = [-0.04347065755731098]
f_Ref = [0.02316782, 0.02057593, 0.0185255, 0.01684338, 0.01542976]  # data for fatigue fitting
f_Ref2 = [
    0.02475444,
    0.02293857,
    0.02330442,
    0.02147316,
    0.02049354,
    0.02062233,
    0.01878019,
    0.01855873,
    0.0184739,
    0.01654167,
    0.01699535,
    0.01664669,
    0.01465995,
    0.01570862,
    0.01502338,
]  # data for direct calculation of f
u_v_Ref = [2.2798066873167544, 2.543927034161581]
Nf_fit = np.power(10, range(2, 7))
strengthRatio = 3.9444444444444446
tensileStrength = 36
material = FrpMaterialProperties().readMaterial(join(programDir, "data", "CFRP_HyMod_old.json"))


def getFatigueParamDf():
    return getMeanstressesAndAmplitudesNormWoehler(A_ref[-3:], B_ref[-3:], R_ref[-3:], Nf_fit, tensileStrength)


def test_readFatigueTestData():
    folder = join(programDir, "test/test_mechanics/fatigue_test_data")
    df = readFatigueTestDataTxtFiles(folder)
    outString = df.to_csv()
    df2 = readFatigueTestData(StringIO(outString))
    # df.to_csv(join(programDir, 'test/test_mechanics/fatigue_test_data/testData.csv'))
    assert np.allclose(df.to_numpy(), df2.to_numpy())


def test_getWoehlerParameters():
    As, Bs, Rs = getWoehlerParameters(testDataFrame)
    assert np.allclose(As, A_ref, rtol=1e-2)
    assert np.allclose(Bs, B_ref, rtol=3e-2)


def test_getMeanstressesAndAmplitudes():
    tensileStrength, compressionStrength = material.getStrengths(22)
    A, B, R = A_ref[-3:], B_ref[-3:], R_ref[-3:]
    df = getMeanstressesAndAmplitudesNormWoehler(A, B, R, Nf_fit, tensileStrength)
    assert np.allclose(df["meanStressNorm"], meanStressNormRef)
    assert np.allclose(df["stressAmplitudeNorm"], stressAmplitudeNormRef)


def test_getFatigueParameters():
    fatigueParamDf = getFatigueParamDf()
    """    fatigueStressfactor, u, v = getFatigueParameters(fatigueParamDf['meanStressNorm'],
                                                         fatigueParamDf['stressAmplitudeNorm'],
                                                         strengthRatio, 3)
    """
    ff = FatigueParamFitter(strengthRatio, len(fatigueParamDf["stressRatio"].unique()))
    fatigueStressfactor, u, v = ff.getFatigueParameters(meanStressNormRef, stressAmplitudeNormRef)
    assert np.allclose(fatigueStressfactor, f_Ref)
    assert np.allclose([u, v], u_v_Ref)


def test_addMeanstressesAndAmplitudesNorm():
    df2 = addMeanstressesAndAmplitudesNorm(testDataFrame[:10].copy(), tensileStrength)
    assert np.allclose(
        df2["stressAmplitudeNorm"],
        [
            15.73055556,
            16.73333333,
            15.525,
            17.84444444,
            29.01388889,
            24.71944445,
            29.49722222,
            28.09722223,
            7.78888889,
            8.25277778,
        ],
    )
    assert np.allclose(
        df2["meanStressNorm"],
        [0.0, 0.0, 0.0, 0.0, 35.46141975, 30.21265433, 36.0521605, 34.34104939, -9.51975309, -10.08672839],
    )


def test_getFatigueStressFactor():
    fatigueParamDf = getFatigueParamDf()
    f = getFatigueStressFactor(
        fatigueParamDf["stressAmplitudeNorm"], fatigueParamDf["meanStressNorm"], strengthRatio, *u_v_Ref
    )
    assert np.allclose(f, f_Ref2)


def test_getMastercurveParameters():
    C, D = fitMastercurveParameters(Nf_fit, np.log10(f_Ref))
    assert np.allclose([C, D], Cs_Ref + Ds_Ref, rtol=2e-2)


def test_fitFatigueParameters():
    resultDf = fitFatigueParameters(testDataFrame, "CFRP_HyMod_old.json", [22])
    assert np.allclose(resultDf["C"], Cs_Ref)
    assert np.allclose(resultDf["D"], Ds_Ref)


def test_fitFatigueParametersAll():
    resultDf = fitFatigueParameters(testDataFrame, "CFRP_HyMod_old.json")
    rtol = 1e-3
    ref = [
        [material.u_11, material.v_11, material.A_11, material.B_11],
        [material.u_22, material.v_22, material.A_22, material.B_22],
    ]
    assert np.allclose(resultDf.loc[:1, ["u", "v", "C", "D"]], ref, rtol=rtol)

    return
    # todo: check this
    rtol = 1e-1
    ref = [
        [material.u_12, material.v_12, material.A_12, material.B_12],
    ]
    assert np.allclose(resultDf.loc[2:, ["u", "v", "C", "D"]], ref, rtol=rtol)
