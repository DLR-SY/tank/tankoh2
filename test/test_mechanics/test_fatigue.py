# SPDX-FileCopyrightText: 2023 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

from os.path import join

import numpy as np
import pandas as pd
import pytest

from tankoh2 import programDir
from tankoh2.design.metal.material import alu6061T6
from tankoh2.mechanics.fatigue import (
    _extendOccurences,
    addCyclesToFailureFRP,
    correctSnParameters,
    getCyclesToFailure,
    getCyclesToFailureAllDirectionsFRP,
    getFatigueLifeFRP,
    getFatigueLifeFRPTankLevel,
    getFatigueLifeMetalTankLevel,
    lessardShokriehRemainingStrength,
    masterCurve,
    masterCurveInv,
    masterCurveLog10,
    stressLifeMinerRule,
)
from tankoh2.mechanics.fatiguefitting import readFatigueTestDataTxtFiles
from tankoh2.mechanics.material import FrpMaterialProperties
from tankoh2.service.exception import Tankoh2Error


def test_getCyclesToFailure():
    # comparison with excel chart scenario #2 first row
    A1, A2, A3, A4 = alu6061T6["SN_parameters"]
    A1, A4 = correctSnParameters(A1, A2, A4, alu6061T6["Kt_curve"], 1.2)
    nf = getCyclesToFailure(187.05, 0.0, A1, A2, A3, A4)
    assert np.allclose(nf, 6.250796e5, rtol=1e-3)


def test_getCyclesToFailure_Fail2():
    # comparison with excel chart scenario #2 first row
    A1, A2, A3, A4 = alu6061T6["SN_parameters"]
    A1, A4 = correctSnParameters(A1, A2, A4, alu6061T6["Kt_curve"], 1.2)
    with pytest.raises(Tankoh2Error):
        getCyclesToFailure(0.0, 187.05, A1, A2, A3, A4)


def test_stressLifeMinerRule():
    # comparison with excel chart scenario #2
    A1, A2, A3, A4 = alu6061T6["SN_parameters"]
    A1, A4 = correctSnParameters(A1, A2, A4, alu6061T6["Kt_curve"], 1.2)
    nf = getCyclesToFailure([187.05, 187.05], [0.0, 115.49], A1, A2, A3, A4)
    occurences = np.array([5000, 50000])
    damage = stressLifeMinerRule(occurences, nf)
    assert np.allclose(damage, 8.2061e-3, rtol=1e-3)


def test_getFatigueLifeMetalAircraftTanks():
    # use scenario 2 of excel sheet
    scatter = 10
    ref = 8.206100e-03 * scatter
    fatigueLife = getFatigueLifeMetalTankLevel(alu6061T6, 187.05, 115.49, 50000 * scatter, 5000 * scatter, 1.2)
    assert np.allclose(fatigueLife, ref, rtol=1e-3)


materialName = join(programDir, "data", "CFRP_HyMod_old.json")
material = FrpMaterialProperties().readMaterial(materialName)
fatigueTestDataDf = readFatigueTestDataTxtFiles(join(programDir, "test/test_mechanics/fatigue_test_data"))


def test_masterCurve():
    C, D = 0.02684, -0.04346
    Nf = np.power(10, range(2, 7))

    f = masterCurve(Nf, C, D)
    Nf_ref = masterCurveInv(f, C, D)
    assert np.allclose(Nf, Nf_ref)

    f2 = np.power(10, masterCurveLog10(Nf, np.log10(C), D))
    Nf_ref2 = masterCurveInv(f2, C, D)
    assert np.allclose(Nf, Nf_ref2)


def test_masterCurveTestData11():
    direction = 11
    df = fatigueTestDataDf.loc[fatigueTestDataDf["direction"] == direction]
    df = addCyclesToFailureFRP(df, material, direction)
    assert np.allclose(np.log10(df["cycles"]), np.log10(df["Nf"]), atol=1.8)


def test_masterCurveTestData22():
    direction = 22
    df = fatigueTestDataDf.loc[fatigueTestDataDf["direction"] == direction]
    df = addCyclesToFailureFRP(df, material, direction)
    assert np.allclose(np.log10(df["cycles"]), np.log10(df["Nf"]), atol=1.5)


def test_masterCurveTestData12():
    direction = 12
    df = fatigueTestDataDf.loc[fatigueTestDataDf["direction"] == direction]
    df = addCyclesToFailureFRP(df, material, direction)
    assert np.allclose(np.log10(df["cycles"]), np.log10(df["Nf"]), atol=2.4)


def test_addCyclesToFailureFRP11():
    # R=0.1, stresses from last test sample
    df = pd.DataFrame([[224.777778, 2247.777778]], columns=["stressLower", "stressUpper"])
    df = addCyclesToFailureFRP(df, material, 11)
    assert np.allclose(df["Nf"], 184.375433)


def test_addCyclesToFailureFRP22():
    # R=10, stresses from first test sample
    df = pd.DataFrame([[-122.22222, -12.222222]], columns=["stressLower", "stressUpper"])
    df = addCyclesToFailureFRP(df, material, 22)
    assert np.allclose(df["Nf"], 456.70487)


class Test_IntegrationFRP:
    maxPressures = np.array([0.5, 0.5])
    minMaxFac1 = 2
    minMaxFac2 = 10
    minPressures = maxPressures / [minMaxFac1, minMaxFac2]
    safetyFac = 2
    material = material

    critCyclesRefIFF = [
        # 11
        [
            np.array([[1.43000441e51, 2.09010352e63], [6.38838747e66, 1.43000441e51], [np.inf, 1.41867866e58]]),
            np.array([[8.91883331e37, 3.43344672e50], [1.98524872e55, 8.91883331e37], [np.inf, 1.60183936e45]]),
        ],
        # 22
        [
            np.array([[1.22997019e09, 5.37184531e21], [7.15720523e31, 1.22997019e09], [np.inf, 1.53090373e17]]),
            np.array([[3.42381794e06, 1.15216779e17], [2.98051027e25, 3.42381794e06], [np.inf, 1.22508624e13]]),
        ],
        # 12
        [
            np.array([[9.55159891e12, np.inf], [1.72110022e09, 9.55159891e12], [9.44069651e11, 1.15674445e17]]),
            np.array([[5.17006805e10, np.inf], [3.46927374e07, 5.17006805e10], [9.49941496e09, 6.09259058e14]]),
        ],
    ]
    critCyclesRefFF = [
        # 11
        [
            np.array([[2.72719403e31, 3.95973283e38], [1.49004106e03, 5.09586918e29], [np.inf, 5.09586918e29]]),
            np.array([[1.34292432e18, 1.07642299e25], [8.33568696e02, 3.70859200e16], [np.inf, 3.70859200e16]]),
        ]
    ]
    refDamageByDirectionElementLayer = np.array(
        [
            [[1.12122288e-36, 2.91252518e-49], [5.03715223e-54, 1.12122288e-36], [0.00000000e00, 6.24282326e-44]],
            [[3.00201883e-05, 8.68115294e-16], [3.35514417e-24, 3.00201883e-05], [0.00000000e00, 8.16922275e-12]],
            [[2.03890503e-09, 0.00000000e00], [3.46347135e-06, 2.03890503e-09], [1.15862080e-08, 1.72778745e-13]],
        ]
    )

    def getPressure2stressDict(self):
        maxStresses = np.array([[600, 300], [-200, 600], [0, 400]])
        maxStresses = maxStresses / self.safetyFac
        stressesMaxPressure = np.array(
            [
                maxStresses,  # 11
                maxStresses / 300 * 20,  # 22
                (maxStresses / 300 * 20) - 10,  # 12
            ]
        )
        pressure2stressDict = {
            self.maxPressures[0]: stressesMaxPressure,
            self.minPressures[0]: stressesMaxPressure / self.minMaxFac1,
            self.minPressures[1]: stressesMaxPressure / self.minMaxFac2,
        }
        return pressure2stressDict

    def test_getCyclesToFailureAllDirectionsFRP_IFF(self):
        # shape (numberOfElements=3, numberOfLayers=2)
        # cylindrical section, mid section, dome section
        pressure2stressDict = self.getPressure2stressDict()
        critCycles = getCyclesToFailureAllDirectionsFRP(
            self.material, self.maxPressures, self.minPressures, pressure2stressDict
        )
        assert np.allclose(critCycles, self.critCyclesRefIFF)

    def test_getCyclesToFailureAllDirectionsFRP_FF(self):
        # for FF(fibrefailure) only check 11 direction

        # shape (numberOfElements=3, numberOfLayers=2)
        # cylindrical section, mid section, dome section
        maxStresses = np.array([[2000, 1300], [-1300, 2200], [0, 2200]])
        maxStresses = maxStresses / self.safetyFac
        p2sigDict = {
            0.5: [maxStresses],  # 11
            0.25: [maxStresses / self.minMaxFac1],  # 11
            0.05: [maxStresses / self.minMaxFac2],  # 11
        }
        critCycles = getCyclesToFailureAllDirectionsFRP(
            self.material, self.maxPressures, self.minPressures, p2sigDict, usedDirections=[11]
        )
        assert np.allclose(critCycles, self.critCyclesRefFF)

    def test_stressLifeFrpIFF(self):
        occurences = np.array([1000, 100])
        # directionCount, cycleCaseCount, numberOfElements, numberOfLayers = self.critCyclesRefIFF.shape
        occurencesArray = np.ones_like(self.critCyclesRefIFF)

        occurencesArraySwap = np.swapaxes(occurencesArray, 1, 3)
        # occurencesArraySwap is only a view on occurencesArray, so no swap back is needed
        occurencesArraySwap *= occurences

        damageRef = np.array(
            [
                [[1.12122288e-35, 2.91252517e-49], [5.03715222e-53, 1.12122288e-36], [0.00000000e00, 6.24282325e-44]],
                [[2.92884633e-04, 8.67947756e-16], [3.35513159e-23, 2.92884633e-05], [0.00000000e00, 8.16334386e-12]],
                [[1.94467997e-08, 0.00000000e00], [2.94055004e-05, 1.94467997e-09], [1.06328885e-07, 1.64998288e-13]],
            ]
        )

        damageByDirElemLayer = stressLifeMinerRule(occurences, self.critCyclesRefIFF, sumAxis=1)
        assert np.allclose(damageByDirElemLayer, damageRef)

    def test_getFatigueLifeFRP(self):
        pressure2stressDict = self.getPressure2stressDict()

        occurences = np.array([1000, 100])
        damageByDirectionElementLayer = getFatigueLifeFRP(
            materialName,
            self.maxPressures,
            self.minPressures,
            occurences,
            None,
            None,
            None,
            None,
            pressure2stressDict=pressure2stressDict,
        )
        assert np.allclose(damageByDirectionElementLayer, self.refDamageByDirectionElementLayer)

    def test_lessardShokriehRemainingStrength(self):
        startingStrength = 2500
        maxStress = 2000
        finalStrength = lessardShokriehRemainingStrength(maxStress, 1000000, 1000000, 0.884, 45, startingStrength)
        assert np.allclose(maxStress, finalStrength)
