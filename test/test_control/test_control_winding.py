import os

import numpy as np
import pytest

from tankoh2 import programDir, pychainIsLoaded, settings
from tankoh2.control.control_winding import createDesign


def setup_module():
    settings.optimizerSeed = 1


def teardown_module():
    settings.optimizerSeed = None


def assertPreviousResult(resultsPreviousFloat, resultsPreviousInt, configFile):
    params = {"configFile": configFile}
    resultsNow = createDesign(**params)
    for resultPrevious in resultsPreviousFloat:
        assert np.allclose(resultsNow[resultPrevious[1]], resultPrevious[2], rtol=0.02)
    for resultPrevious in resultsPreviousInt:
        assert resultsNow[resultPrevious[1]] - resultPrevious[2] < 1


@pytest.mark.skipif(not pychainIsLoaded, reason="pychain must be loaded for this test")
def test_PreviousHighPressureTank():
    "Yaml-File and log-File are in the folder PreviousHighPressureTank in the folder PreviousResults"
    resultsPreviousFloat = [
        ("shellMass", 0, 4.4028671318028625),
        ("volume", 5, 68.74278437501195),
        ("area", 6, 1.2467250020194367),
        ("lengthAxial", 7, 1608.384125),
        ("cylinderThickness", 9, 2.25),
        ("maxThickness", 10, 2.6850000000000014),
        ("reserveFactor", 11, 0.17289147856625314),
        ("gravimetricIndex", 12, 0.2513383310349205),
    ]  # (VariableName, Index in Results, Previous Result)
    resultsPreviousInt = [("numberOfLayers", 8, 9)]  # (VariableName, Index in Results, Previous Result)
    assertPreviousResult(
        resultsPreviousFloat,
        resultsPreviousInt,
        os.path.join(
            programDir,
            "test",
            "test_control",
            "PreviousResults",
            "PreviousHighPressureTank",
            "PreviousHighPressureTank.yaml",
        ),
    )


@pytest.mark.skipif(not pychainIsLoaded, reason="pychain must be loaded for this test")
def test_PreviousCryoTank1():
    "Yaml-File and log-File are in the folder PreviousCryoTank1 in the folder PreviousResults"
    resultsPreviousFloat = [
        ("shellMass", 0, 3.418541272119498),
        ("volume", 5, 779.0140219233026),
        ("area", 6, 4.592421973046178),
        ("lengthAxial", 7, 1680.049774),
        ("cylinderThickness", 9, 0.5),
        ("maxThickness", 10, 5.462499999999906),
        ("reserveFactor", 11, 1.140699574850146),
        ("gravimetricIndex", 12, 0.9364047228609323),
    ]  # (VariableName, Index in Results, Previous Result)
    resultsPreviousInt = [("numberOfLayers", 8, 2)]  # (VariableName, Index in Results, Previous Result)
    assertPreviousResult(
        resultsPreviousFloat,
        resultsPreviousInt,
        os.path.join(
            programDir, "test", "test_control", "PreviousResults", "PreviousCryoTank1", "previousCryoTank1.yaml"
        ),
    )


@pytest.mark.skipif(not pychainIsLoaded, reason="pychain must be loaded for this test")
def test_PreviousCryoTank2():
    "Yaml-File and log-File are in the folder PreviousCryoTank2 in the folder PreviousResults"
    resultsPreviousFloat = [
        ("shellMass", 0, 46.871965218745025),
        ("volume", 5, 5883.902339970661),
        ("area", 6, 20.01094021802915),
        ("lengthAxial", 7, 4661.244452000001),
        ("cylinderThickness", 9, 1.5),
        ("maxThickness", 10, 8.90750000000001),
        ("reserveFactor", 11, 1.159799115712877),
        ("gravimetricIndex", 12, 0.8929994848593286),
    ]  # (VariableName, Index in Results, Previous Result)
    resultsPreviousInt = [("numberOfLayers", 8, 6)]  # (VariableName, Index in Results, Previous Result)
    assertPreviousResult(
        resultsPreviousFloat,
        resultsPreviousInt,
        os.path.join(
            programDir, "test", "test_control", "PreviousResults", "PreviousCryoTank2", "previousCryoTank2.yaml"
        ),
    )
