import numpy as np

from tankoh2.control.control_metal import createDesign


def test_default_metal_tank(tmpdir):
    testDir = str(tmpdir)
    params = {"materialName": "alu2219", "domeType": "circle", "runDir": testDir}
    results1 = createDesign(**params)
    params = {"materialName": "alu2219", "domeType": "circle", "runDir": testDir, "configFile": "defaults.yaml"}
    results2 = createDesign(**params)

    assert results1[:-1] == results2[:-1]
