# General
windingOrMetal: winding  # Switch between winding mode or metal design [winding, metal]
tankname: tank_name # Name of the tank
nodeNumber: 500 # node number along the contour
verbose: false # More console output
verbosePlot: false # Plot the optimization target function
configFile: # Config file to be used, which can be modified by further parameters
help: '' # show this help message and exit
# Optimization
maxLayers: 100 # Maximum number of layers to be added
relRadiusHoopLayerEnd: 0.95 # relative radius (to cyl radius) where hoop layers end [-]
targetFuncWeights: [1.0, 0.2, 1.0, 0, 0.25, 0.1] # Weights to the target function constituents: maxPuck, maxCritPuck, sumPuck, layerMass, maxStrainDiff, maxCritStrainDiff
# Geometry
dcyl: 400 # Diameter of the cylindrical section [mm]. Can be automatically adapted if volume is given
lcyl: 500 # Length of the cylindrical section [mm]. Can be automatically adapted if volume is given
lcylByR: 2.5 # only if lcyl is not given [-]
volume: # Volume requirement [m**3]. If it does not fit to other geometry parameters, l_cyl is adapted and if l_cly would be below 150mm d_cyl is adapted
polarOpeningRadius: 20 # Polar opening radius [mm]
h2Mass: # H2 Mass, defines volume if given & no volume given
# Geometry_Dome
domeType: isotensoid # Shape of dome geometry [isotensoid, isotensoid_MuWind, circle, ellipse, torispherical, conical***, custom]
domeContour: [null, null] # Must be given if domeType==custom. X- and R-array should be given without whitespaces like "[x1,x2],[r1,r2]" in [mm]
domeLengthByR: 0.5 # Axial length of the dome. Only used for domeType==ellipse [mm]
alpha: 0.5 # ratio of the difference of cylindrical and small diameter to the cylindrical diameter
beta: 1.5 # ratio of sum of the radial and conical length to the cylindrical diameter
gamma: 0.5 # ratio of the radial length to the sum of radial and conical length
delta1: 0.5 # ratio of the semi axes of the elliptical dome end
r1ToD0: 0.8 # ratio of the radius of the dish to the diameter of the cylinder (for torispherical domes)
r2ToD0: 0.154 # ratio of the radius of the knuckle to the diameter of the cylinder (for torispherical domes)
# Geometry_Dome2
dome2Type: # Shape of dome geometry [isotensoid, isotensoid_MuWind, circle, ellipse, torispherical, custom]
dome2Contour: [null, null] # Must be given if domeType==custom. X- and R-array should be given without whitespaces like "[x1,x2],[r1,r2]" in [mm]
dome2LengthByR: 0.5 # Axial length of the dome. Only used for domeType==ellipse [mm]
# Fitting
fittingType: A # fitting type (A, B, or custom)
r0: # valve radius  [mm]
r1: # smaller disc radius [mm]
rD: # wider disc radius [mm]
dXB: # boss length [mm]
dX1: # disc length [mm]
lV: # valve length [mm]
r2: # boss radius (only for type B) [mm]
r3: # wider boss section radius (only type B) [mm]
dX2: # distance from disc to wider boss section (only type B)[mm]
alphaP: # angle of boss widening (only type B) - 0� is steepest, 90� flattest [�]
rP: # radius of curvature of rounded section (only type B) [mm]
customBossName: # Name of data file specifying custom boss contour as  [x y] differences starting from the polar opening
# Design
safetyFactor: 2 # Safety factor used in design [-]
valveReleaseFactor: 1.0 # Factor defining additional pressure to account for the valve pressure inaccuracies
temperature: # Temperature, for differentiating between cryogenic and compressed storage and finding density [K]
pressure: 1.0 # Operational pressure [MPa]
minPressure: 0.1 # Minimal operational pressure [MPa]
maxFill: 0.9 # Max fill level for liquid storage
useHydrostaticPressure: false # Flag whether hydrostatic pressure according to CS 25.963 (d) should be applied
tankLocation: wing_at_engine # Location of the tank according to CS 25.963 (d). Only used if useHydrostaticPressure. Options: [wing_no_engine, wing_at_engine, fuselage]
initialAnglesAndShifts: # 2 lists defining angles and shifts used before optimization starts
# Material
materialName: CFRP_HyMod # For metal tanks: name of the material defined in tankoh2.design.metal.material. For wound tanks: name of the .json for a �Wind material definiton (e.g. in tankoh2/data/CFRP_HyMod.json). If only a name is given, the file is assumed to be in tankoh2/data
failureMode: fibreFailure # Use pucks failure mode [fibreFailure, interFibreFailure]
# Fiber roving parameters
layerThk: 0.125 # Thickness of layers [mm]
layerThkHoop: # Thickness of hoop (circumferential) layers [mm]
layerThkHelical: # Thickness of helical layers [mm]. If None, layerThkHoop is used
rovingWidth: 3.175 # Width of one roving [mm]
rovingWidthHoop: # Width of one roving in hoop layer [mm]
rovingWidthHelical: # Width of one roving in helical layer [mm]. If None, rovingWidthHoop is used
numberOfRovings: 4 # Number of rovings (rovingWidthHoop*numberOfRovings=bandWidth)
tex: 446 # tex number [g/km]
fibreDensity: 1.78 # Fibre density [g/cm^3]
# Fatigue parameters
pressureMin: 0.1 # Minimal operating pressure [MPa]
cycles: 50000 # Number of operational cycles from pressureMin to pressure [-]
heatUpCycles: 100 # Number of cycles to ambient T and p [-]
simulatedLives: 5 # Number of simulated lifes (scatter) [-]
Kt: 5.0 # Stress concentration factor [-]
# Aux Materials
linerThickness: 0.0 # Thickness of the liner [mm]
insulationThickness: 0.0 # Thickness of the insluation [mm]
fairingThickness: 0.0 # Thickness of the fairing [mm]
linerMaterial: PVDF # Liner Material Name (in massdata.py)
insulationMaterial: Rohacell41S # Insulation Material Name (in massdata.py)
fairingMaterial: Kevlar # Fairing Material Name (in massdata.py)
