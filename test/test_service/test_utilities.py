# SPDX-FileCopyrightText: 2023 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

import os

import numpy as np

from tankoh2 import programDir
from tankoh2.design.existingdesigns import defaultDesign
from tankoh2.design.loads import getHydrostaticPressure
from tankoh2.service.physicalprops import rhoLh2ByTSaturation
from tankoh2.service.utilities import readParametersFromYAML, writeDefaultsToYAML, writeParametersToYAML

filename = "test_YAML_defaults.yaml"


def teardown_function(function):
    if function == test_write_YAML:
        os.remove(os.path.join(programDir, filename))


def test_getHydrostaticPressure():
    locLengthHeightBaffles = [
        ("fuselage", 1, 1, None),
        ("fuselage", 3, 1, 1),
        ("fuselage", 0.5, 1.5, None),
        ("wing_no_engine", 2, 1, None),
        ("wing_no_engine", 1, 1.5, None),
        ("wing_no_engine", 3, 1, 2),
        ("wing_at_engine", 1, 1, None),
        ("wing_at_engine", 0.5, 1.5, None),
    ]
    r = []
    pressure = 0.1
    for loc, length, height, baffle in locLengthHeightBaffles:
        r.append(getHydrostaticPressure(loc, length, height, pressure, baffleDist=baffle))
    r = np.array(r)
    assert np.all(np.abs(r - r[0] < 1e-8))  # check if all equal


def test_write_YAML():
    filepath = os.path.join(programDir, filename)
    writeParametersToYAML(defaultDesign, filepath)
    defaultsFromWriteFunction = readParametersFromYAML(filepath)
    assert defaultDesign == defaultsFromWriteFunction


def test_read_YAML():
    defaultsFromReadFunction = readParametersFromYAML(os.path.join(programDir, "designs", "defaults.yaml"))
    assert (
        defaultDesign == defaultsFromReadFunction
    ), "If you changed the defaults, update defaults.yaml using the function tankoh2.utilities.writeDefaultsToYAML()"
