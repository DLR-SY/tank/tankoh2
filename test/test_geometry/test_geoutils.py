# SPDX-FileCopyrightText: 2023 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

import numpy as np

from tankoh2.geometry.geoutils import getRadiusByShiftOnContour

radii = np.array(
    [
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.38,
        200.34716508,
        200.25226305,
        202.67695305,
        202.46422094,
        202.2001596,
        201.88816428,
        201.53099691,
        201.13030831,
        200.68840747,
        200.20670645,
        199.68642854,
        199.12813147,
        198.53395145,
        197.90325502,
        197.23691013,
        196.53569404,
        195.79895138,
        195.02775381,
        194.22213022,
        193.3821205,
        192.50820845,
        191.59962846,
        190.65733576,
        189.68139435,
        188.67184713,
        187.62915129,
        186.55349651,
        185.44465341,
        184.30568673,
        183.1340055,
        181.93120062,
        180.69953763,
        179.43609656,
        178.14482137,
        176.82552728,
        175.4787572,
        174.10548212,
        172.7062694,
        171.28210331,
        169.83355861,
        168.36161499,
        166.86817575,
        165.35011848,
        163.81294604,
        162.25241619,
        160.67554892,
        159.07742526,
        157.46168444,
        155.82845178,
        154.17674551,
        152.5104212,
        150.82587404,
        149.12812709,
        147.4139505,
        145.68602083,
        143.94563817,
        142.19057927,
        140.42349332,
        138.64442528,
        136.85365323,
        135.05194982,
        133.23949115,
        131.41709181,
        129.58491807,
        127.74373349,
        125.89364288,
        124.03560879,
        122.16970129,
        120.29623202,
        118.41598916,
        116.52864633,
        114.63615589,
        112.73748124,
        110.83327402,
        108.92416261,
        107.01038464,
        105.09206898,
        103.16985969,
        101.24389367,
        99.31433917,
        97.38162238,
        95.44597505,
        93.50761971,
        91.56655273,
        89.62336423,
        87.67801237,
        85.73072975,
        83.78147069,
        81.83073201,
        79.87732341,
        77.92483552,
        75.96989888,
        74.0136938,
        72.05657711,
        70.10043733,
        68.13956491,
        66.17988581,
        64.21954375,
        62.2576646,
        60.29706228,
        58.33526735,
        56.37322575,
        54.41099717,
        52.45015929,
        50.48588435,
        48.52415441,
        46.56220465,
        44.6005957,
        42.63943519,
        40.67882427,
        38.71886199,
        36.75964547,
        34.80126096,
        32.84378681,
        30.88729188,
        28.93182804,
        26.97743608,
        25.02225493,
        23.07155473,
    ]
)

lengths = np.array(
    [
        0.0,
        1.9379845,
        3.87596899,
        5.81395349,
        7.75193798,
        9.68992248,
        11.62790698,
        13.56589147,
        15.50387597,
        17.44186047,
        19.37984496,
        21.31782946,
        23.25581395,
        25.19379845,
        27.13178295,
        29.06976744,
        31.00775194,
        32.94573643,
        34.88372093,
        36.82170543,
        38.75968992,
        40.69767442,
        42.63565891,
        44.57364341,
        46.51162791,
        48.4496124,
        50.3875969,
        52.3255814,
        54.26356589,
        56.20155039,
        58.13953488,
        60.07751938,
        62.01550388,
        63.95348837,
        65.89147287,
        67.82945736,
        69.76744186,
        71.70542636,
        73.64341085,
        75.58139535,
        77.51937984,
        79.45736434,
        81.39534884,
        83.33333333,
        85.27131783,
        87.20930233,
        89.14728682,
        91.08527132,
        93.02325581,
        94.96124031,
        96.89922481,
        98.8372093,
        100.7751938,
        102.71317829,
        104.65116279,
        106.58914729,
        108.52713178,
        110.46511628,
        112.40310078,
        114.34108527,
        116.27906977,
        118.21705426,
        120.15503876,
        122.09302326,
        124.03100775,
        125.96899225,
        127.90697674,
        129.84496124,
        131.78294574,
        133.72093023,
        135.65891473,
        137.59689922,
        139.53488372,
        141.47286822,
        143.41085271,
        145.34883721,
        147.28682171,
        149.2248062,
        151.1627907,
        153.10077519,
        155.03875969,
        156.97674419,
        158.91472868,
        160.85271318,
        162.79069767,
        164.72868217,
        166.66666667,
        168.60465116,
        170.54263566,
        172.48062016,
        174.41860465,
        176.35658915,
        178.29457364,
        180.23255814,
        182.17054264,
        184.10852713,
        186.04651163,
        187.98449612,
        189.92248062,
        191.86046512,
        193.79844961,
        195.73643411,
        197.6744186,
        199.6124031,
        201.5503876,
        203.48837209,
        205.42635659,
        207.36434109,
        209.30232558,
        211.24031008,
        213.17829457,
        215.11627907,
        217.05426357,
        218.99224806,
        220.93023256,
        222.86821705,
        224.80620155,
        226.74418605,
        228.68217054,
        230.62015504,
        232.55813953,
        234.49612403,
        236.43410853,
        238.37209302,
        240.31007752,
        242.24806202,
        244.18604651,
        246.12403101,
        248.0620155,
        250.0,
        251.9647098,
        253.92869135,
        257.19827918,
        259.22831876,
        261.25563802,
        263.27835861,
        265.2966269,
        267.31417782,
        269.32873422,
        271.34130559,
        273.35240351,
        275.36399514,
        277.37157844,
        279.38001297,
        281.3881926,
        283.39510723,
        285.40371141,
        287.41164697,
        289.41950444,
        291.42766101,
        293.43527129,
        295.44451875,
        297.45348394,
        299.46252137,
        301.47195229,
        303.48135806,
        305.4910113,
        307.50188838,
        309.50937539,
        311.5188537,
        313.52851057,
        315.53554673,
        317.54578784,
        319.55389615,
        321.5613804,
        323.56846267,
        325.5748586,
        327.5807899,
        329.58598594,
        331.5906654,
        333.5945728,
        335.59614422,
        337.60057045,
        339.60150073,
        341.60543028,
        343.60423342,
        345.60510513,
        347.60427139,
        349.6024647,
        351.60162262,
        353.59789096,
        355.5962863,
        357.59158971,
        359.58826249,
        361.58386016,
        363.57751784,
        365.57242394,
        367.56612951,
        369.55919582,
        371.55185683,
        373.54381793,
        375.53540952,
        377.52626184,
        379.51670406,
        381.50639568,
        383.49571803,
        385.48409439,
        387.4719232,
        389.45930844,
        391.44584881,
        393.43231219,
        395.41704239,
        397.40153103,
        399.38547021,
        401.36856453,
        403.35092431,
        405.33274714,
        407.31368557,
        409.29390884,
        411.27353309,
        413.25239427,
        415.23052087,
        417.2079338,
        419.18486768,
        421.16094302,
        423.13641177,
        425.11123286,
        427.08563318,
        429.05928131,
        431.0335412,
        433.00493543,
        434.97700763,
        436.94869618,
        438.9197648,
        440.88843,
        442.86054457,
        444.83024943,
        446.7995092,
        448.76929285,
        450.73686281,
        452.70478076,
        454.67217583,
        456.63906301,
        458.60393231,
        460.57168773,
        462.53639961,
        464.50089744,
        466.46467555,
        468.42767816,
        470.38985205,
        472.35114189,
        474.31149031,
        476.27084681,
        478.22916489,
        480.18640401,
        482.1425374,
        484.09754659,
        486.05331347,
        488.00458089,
    ]
)


def test_getRadiusByShiftOnContour():
    assert np.allclose(23.00058139, getRadiusByShiftOnContour(radii, lengths, 25, 2))


if __name__ == "__main__":
    from matplotlib import pyplot as plt

    fig, ax = plt.subplots()
    testRadii = np.linspace(23, 60)
    ax.plot(testRadii, [getRadiusByShiftOnContour(radii, lengths, startR, 2) for startR in testRadii], linewidth=2.0)
    plt.show()
