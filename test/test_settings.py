# SPDX-FileCopyrightText: 2023 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

import pytest

from tankoh2 import pychain, pychainIsLoaded


def test_pychainLoaded():
    winding = pychain.winding
    if pychainIsLoaded:
        # if loaded, pychain should be the read pychain implementation and return the proper winding package
        assert winding is not None
    else:
        # if not loaded, pychain should be tankoh2.settings.PychainMock and always return None
        assert winding is None


@pytest.mark.skipif(not pychainIsLoaded, reason="pychain must be loaded for this test")
def test_pychainLoaded2():
    winding = pychain.winding
    assert winding is not None
