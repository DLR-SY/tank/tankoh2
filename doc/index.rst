.. SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
..
.. SPDX-License-Identifier: MIT

tankoh2 documentation
=====================


.. only:: html

    :Release: |version|
    :Date: |today|


-----
Scope
-----
tankoh2 is analyses, designs and optimizes composite and metallic tanks for the high-pressure and
cryogenic storage of hydrogen (or other media).
This documentation outlines the usage and optimization philosophy of the tool.

---------
Reference
---------

.. toctree::
   :maxdepth: 2

   usage
   optimizationWorkflow
   optimizationOptions
   optimizationFunctions
   bending
   fatigue_cfrp
   fatigue_metal
   metal
   tankoh2





Indices and tables
------------------

used bib

.. bibliography::
   :cited:

unused bib

.. bibliography::
   :notcited:



.. raw:: latex

   \listoffigures
   \listoftables

.. only:: html

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
