===================
Metal Fatigue Model
===================

Fatigue analysis is carried out in three steps:

- establishing the fatigue parameters in getFatigueLifeMetal
- calculating the number of cycles to failure in getCyclesToFailure
- applying the miner rule to the critical cycles and occuring cycles



.. autofunction:: tankoh2.mechanics.fatigue.getFatigueLifeMetal
.. autofunction:: tankoh2.mechanics.fatigue.getCyclesToFailure
.. autofunction:: tankoh2.mechanics.fatigue.stressLifeMinerRule
