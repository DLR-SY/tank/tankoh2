Usage
=====

Tankoh2 can be run from the command line using

.. code-block:: bash

    cd <path_to_tankoh2>/src
    python -m tankoh2 [options]

Parameters can be set as command-line arguments or through config files. Parameters not set will use the default values.


Config files
------------
It's possible to write design config files in .yaml format to define any
parameters for the winding or metal simulation. Using config files, it's possible to reproduce and modify runs.
Config files need to be put in the folder ``tankoh2/designs`` and are used via the option ``--configFile [nameOfFile.yaml]``.
Any non-default parameters can be defined in parameter:value pairs.
All possible parameters with descriptions can be seen in ``tankoh2/designs/defaults.yaml``.
Flags can be turned on or off through the values true or false.
Config files can also recursively include other configFiles as parameters, whose parameters can be overwritten.
The parameters which were used in each run are also saved in .yaml format in the run directory.
These directories are located in the directory ``tankoh2/tmp`` by default or speciefied by the option runDir.
Parameters given in config files are overwritten by parameters supplied explicitly via
command line options. The option --windingOrMetal can currently not be defined in a
config file and must be given explicitly for metal simulation (default is winding). Example::

	tankname: example
	domeType: isotensoid
	pressure: 35
	h2Mass: 5.5
	temperature: 293
	polarOpeningRadius: 40
	dcyl: 400
	targetFuncWeights: [1.0, 0.2, 1.0, 0, 0.5, 0.4, 0.01]
	verbosePlot: true

Parameters
----------
The full list of available parameters and their defaults can also be requested through

.. code-block:: bash

    cd <path_to_tankoh2>/src
    python -m tankoh2 --help

General:
  --windingOrMetal <WINDINGORMETAL>		Switch between winding mode or metal design [winding, metal] (default: winding)
  --tankname <name>       				Name of the tank (default: tank_name)
  --nodeNumber <number>  				number of nodes along the contour (default: 500)
  --verbose          					More console output (default: False)
  --verbosePlot   				        Plot the optimization target function (default: False)
  --configFile <CONFIGFILE>				Config file to be used, which can be modified by further parameters (default: None)
  --help                				show this help message and exit (default: )
  --runDir <RUNDIR>						directory to save run (default: None)

Optimization:
  --maxLayers <layers>                              Maximum number of layers to be added (default: 200)
  --relRadiusHoopLayerEnd <RELRADIUSHOOPLAYEREND>   relative radius (to cyl radius) where hoop layers end [-] (default: 0.95)
  --targetFuncWeights <tfWeights> 	                Weights to the target function constituents: maxPuck, maxCritPuck, sumPuck, layerMass, maxStrainDiff, maxCritStrainDiff, smoothness (default: [1.0, 0.2, 1.0, 0.0, 0.25, 0.1, 0.01])
  --enforceWindableContour                          Modify target function to prefer layers that fulfill contour windability criteria: limit buildup and discourage concave surfaces near fitting (default: False)
  --hoopShiftRange HOOPSHIFTRANGE                   number of hoop layers which are linearly spaced between the maximum and minimum hoop Shift. After this number of layers, the pattern is repeated. If set to 0 (default), then all layers are evenly spread out. (default: 0)
  --hoopLayerCluster HOOPLAYERCLUSTER               number of hoop layers which are created as one cluster during the optimization. (default: 1
  --sortLayers                                      Sort Helical Layers by rising winding angle after adding each layer (default: False)
  --sortLayersAboveAngle SORTLAYERSABOVEANGLE       Angle above which layers should be sorted and moved to the outside, when sortLayers is set to True. (default: 0.0)
  --optimizeHoopShifts                              Optimize the linear hoop shift distributions of the hoop layer patterns (default: False)


Geometry:
  --dcyl <d_cyl>          Diameter of the cylindrical section [mm]. Can be automatically adapted if volume is given (default: 400)
  --lcyl <l_cyl>          Length of the cylindrical section [mm]. Can be automatically adapted if volume is given (default: 500)
  --lcylByR <LCYLBYR>     only if lcyl is not given [-] (default: 2.5)
  --volume <VOLUME>       Volume requirement [m**3]. If it does not fit to other geometry parameters, l_cyl is adapted and if l_cly would be below 150mm d_cyl is adapted (default: None)
  --polarOpeningRadius <r_po>		Polar opening radius [mm] (default: 20)
  --h2Mass <H2MASS>       H2 Mass, defines volume if given & no volume given (default: None)

Geometry_Dome:
  --domeType <DOMETYPE>					Shape of dome geometry [isotensoid, isotensoid_MuWind, circle, ellipse, torispherical, conical***, custom] (default: isotensoid)
  --domeContour <[x, r]>	 			Must be given if domeType==custom. X- and R-array should be given without whitespaces like "[x1,x2],[r1,r2]" in [mm] (default: [None, None])
  --domeContourFile <DOMECONTOURFILE>	Alternative to specify a custom dome contour. A text file in /data which contains two space seperated rows (xand r values). Overwritten by domeContour if defined. (default: None)
  --domeLengthByR <l/r_cyl>				Axial length of the dome. Only used for domeType==ellipse [mm] (default: 0.5)
  --alpha <(dcyl - dsmall)/dcyl> 		ratio of the difference of cylindrical and small diameter to the cylindrical diameter (default: 0.5)
  --beta <(lrad + lcone)/dcyl> 			ratio of sum of the radial and conical length to the cylindrical diameter (default: 1.5)
  --gamma <lrad/(lrad + lcone)> 		ratio of the radial length to the sum of radial and conical length (default: 0.5)
  --delta1 <ldome/rsmall> 				ratio of the semi axes of the elliptical dome end (default: 0.5)
  --r1ToD0 <r1/(2*r_cyl)> 				ratio of the radius of the dish to the diameter of the cylinder (for torispherical domes) (default: 0.8)
  --r2ToD0 <r2/(2*r_cyl)>			 	ratio of the radius of the knuckle to the diameter of the cylinder (for torispherical domes) (default: 0.154)

Geometry_Dome2:
  --dome2Type <DOME2TYPE> 		 			Shape of dome geometry [isotensoid, isotensoid_MuWind, circle, ellipse, torispherical, custom] (default: None)
  --dome2Contour <x, r>						Must be given if domeType==custom. X- and R-array should be given without whitespaces like "[x1,x2],[r1,r2]" in [mm] (default: [None, None])
  --dome2ContourFile <DOME2CONTOURFILE>		Alternative to specify a custom dome contour for dome 2. A text file in /data which contains two space seperated rows (x and r values). Overwritten by domeContour if defined. (default: None)
  --dome2LengthByR <l/r_cyl> 				Axial length of the dome. Only used for domeType==ellipse [mm] (default: 0.5)

Fitting:
  --fittingType FITTINGTYPE 	fitting type (A, B, or custom) (default: A)
  --r0 <r0>               valve radius [mm] (default: None)
  --r1 <r1>               smaller disc radius [mm] (default: None)
  --rD <rD>               wider disc radius [mm] (default: None)
  --dXB <dXB>             boss length [mm] (default: None)
  --dX1 <dX1>             disc length [mm] (default: None)
  --lV <lV>               valve length [mm] (default: None)
  --r2 <r2>               boss radius (only for type B) [mm] (default: None)
  --r3 <r3>               wider boss section radius (only type B) [mm] (default: None)
  --dX2 <dX2>             distance from disc to wider boss section (only type B)[mm] (default: None)
  --alphaP <alphaP>       angle of boss widening (only type B) - 0° is steepest, 90° flattest [°] (default: None)
  --rP <alphaP>           radius of curvature of rounded section (only type B) [mm] (default: None)
  --customBossName <CUSTOMBOSSNAME>  Name of data file specifying custom boss contour as [x y] differences starting from the polar opening (default: None)

Design:
  --safetyFactor <S>      Safety factor used in design [-] (default: 2)
  --valveReleaseFactor <f_pv>		Factor defining additional pressure to account for the valve pressure inaccuracies (default: 1.0)
  --temperature <TEMPERATURE>		Temperature, for differentiating between cryogenic and compressed storage and finding density [K] (default: None)
  --pressure <p_op>       Operational pressure [MPa] (default: 1.0)
  --minPressure <p_op_min>	Minimal operational pressure [MPa] (default: 0.1)
  --maxFill <p_b>         Max fill level for liquid storage (default: 0.9)
  --useHydrostaticPressure	Flag whether hydrostatic pressure according to CS 25.963 (d) should be applied (default: False)
  --tankLocation <loc>    Location of the tank according to CS 25.963 (d). Only used if useHydrostaticPressure. Options: [wing_no_engine,wing_at_engine, fuselage] (default: wing_at_engine)
  --initialAnglesAndShifts <angles, shifts_left, shifts_right> 	3 lists defining angles and (left and right) shifts used before optimization starts (default: None)

Material:
  --materialName <name>   For metal tanks: name of the material defined in tankoh2.design.metal.material. For wound tanks: name of the .json for a µWind material definiton (e.g. in tankoh2/data/CFRP_HyMod.json). If only a name is given, the file is assumed to be in tankoh2/data (default: CFRP_HyMod)
  --failureMode <mode>    Use pucks failure mode [fibreFailure, interFibreFailure] (default: fibreFailure)

Fiber roving parameters:
  --layerThk <thk>        Thickness of layers [mm] (default: 0.125)
  --layerThkHoop <thk>    Thickness of hoop (circumferential) layers [mm] (default: None)
  --layerThkHelical <thk>			Thickness of helical layers [mm]. If None, layerThkHoop is used (default: None)
  --rovingWidth <witdh>   			Width of one roving [mm] (default: 3.175)
  --rovingWidthHoop <witdhHoop>		Width of one roving in hoop layer [mm] (default: None)
  --rovingWidthHelical <witdhHelical>	Width of one roving in helical layer [mm]. If None, rovingWidthHoop is used (default: None)
  --numberOfRovings <NUMBER>	Number of rovings (rovingWidthHoop*numberOfRovings=bandWidth) (default: 4)
  --tex <TEX>             tex number [g/km] (default: 446)
  --fibreDensity <FIBREDENSITY>		Fibre density [g/cm^3] (default: 1.78)

Fatigue parameters:
  --pressureMin <p_min>   Minimal operating pressure [MPa] (default: 0.1)
  --cycles <CYCLES>       Number of operational cycles from pressureMin to pressure [-] (default: 10000)
  --heatUpCycles <HEATUPCYCLES>			Number of cycles to ambient T and p [-] (default: 100)
  --simulatedLives <SIMULATEDLIVES>		Number of simulated lifes (scatter) [-] (default: 5)
  --Kt <Kt>               Stress concentration factor [-] (default: 5.0)

Aux Materials:
  --linerThickness <linerThk>		Thickness of the liner [mm] (default: 0.0)
  --insulationThickness <insThk>	Thickness of the insluation [mm] (default: 0.0)
  --fairingThickness <fairingThk>	Thickness of the fairing [mm] (default: 0.0)
  --linerMaterial <linerMat>		Liner Material Name (in massdata.py) (default: PVDF)
  --insulationMaterial <insMat>		Insulation Material Name (in massdata.py) (default: Rohacell41S)
  --fairingMaterial <fairingMat>	Fairing Material Name (in massdata.py) (default: Kevlar)


Results
-------
Unless otherwise specified, the results of an optimization run are saved in a sub folder
of ``tankoh2/tmp``. The folder includes the following files:

all_parameters_and_results.txt
	The defined non-default arguments, full list of arguments, and a collection of results of the optimization (if it finished)
anglesAndShifts.yaml
	The angles and hoop shifts of the current design (updated throughout the optimization). They are written in .yaml format, so they can be copied into a config file, for example to restart a failed optimization.
backup.vessel
	Current state of the design (updated throughout the optimization). Can be opened within muwind.
contour.png
	Image of the dome contour
[tank_name].design
	The composite layup used by muwind for the tank in json format.
[tank_name].liner
	The liner used by muwind for the tank in json format.
[tank_name].vessel
	The vessel (liner + composite) used by muwind in json format.
[tank_name].wresults
	The winding results generated by muwind in json format.
[tank_name].yaml
	The yaml config file which includes the parameters set for this run, and allows rerunning or modifying the optimization.
[tank_name]LayupBook.txt
	Information about the composite layup in readable format.
domeContour.yaml
	The created dome contour in yaml format, which can be added to a config file to use the same contour again.
elementalResults.csv
	The element-wise FEM results of the final tank simulation. Includes thickness, fiber angle, puck failure indices, and stresses by layer for each element.
nodalResults.csv
	The positions (x and r coordinates) of the nodes which were created in the final winding simulation and used for FEM analysis.
puck_[layerNr].png
	Shows the current puck fiber failure or inter fiber failure indices for all layers, with the first 2 and last 10 layers individually colored. If the option "verbosePlot" is activated, also shows the target function and the optimizers angle choice.
	Parameters
sig_eps_puck.png
	Show the strains, stresses, and puck failure indexes at the end of the optimization.
thicknesses
	Shows the cumulated thicknesses of the composite along the contour.
