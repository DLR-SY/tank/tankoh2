Optimization Workflow
=====================


.. mermaid::

    graph TD
    A((Start with\nInitial Layers))
    subgraph H[tankoh2 design loop]
        direction TB
        B[Add Layer]
        subgraph G[Optimize Layer Angle]
            direction TB
            C1[Setup Target Function]
            subgraph F[Global Optimization Step]
                direction TB
                C2[Solve Tank FEM Model]
                C3[Obtain Mechanical Stresses]
                C4[Scale Hoop Stresses with Analytical Method *]
                C5[Assess Target Function Value]
            end
            C6{Opt found?}
            D[Adjust Angle for Valid Winding Pattern *]
            E[Postprocessing]
        end
        A1{"Failure Crit.\n satisfied?"}
    end
    F(((Finished)))
    A --> B
    A1 -->|no| B
    B --> G
    C1 --> F
    C2 --> C3
    C3 --> C4
    C4 --> C5
    F --> C6
    C6 -->|no| C1
    C6 -->|yes| D
    D --> E
    G --> A1
    A1 -->|yes| F

Note: Scaling Hoop Stresses with Analytical Method
-----------------------------------------------------
A disadvantage of the shell models used for the FEM analysis is that they do not
consider the uneven stress distribution between circumferential layers for
thick shells, with inner layers displacing further and carrying higher loads than outer layers.
This is solved by also calculating the cylindrical stresses according to an analytical solution
of thick axisymmetric composite cylinders (through the MuWind Tube Module) and scaling the
FEM results of the hoop layers by these results. The stresses in the helical layers
are currently still unaffected. On the left, we can see that for an example 700-bar tank with a
diameter of 400 mm, the maximum hoop stress is underestimated by 13% without using the thick
shell rescaling.

|withoutScaling| |withScaling|

.. |withoutScaling| image:: images/optimization/noThickShellHoops.png
	:width: 45 %
.. |withScaling| image:: images/optimization/thickShellHoops.png
	:width: 45 %

Note: Adjust Angle for Valid Winding Pattern
-----------------------------------------------------
During the initial optimization of the angle, valid winding patterns, which lead
to complete and even coverage of the mandrel, are not considered. Instead, a
homogenized approach is used for each layer. After the optimization, the angle
is readjusted to the nearest valid winding angle. The difference is generally
much less than 1°, so the overall optimization progress is not impacted. If a valid
winding angle cannot be found (for example, if a fitting layer would lose contact with
the fitting or the polar opening become too small), the angle is kept as it was, but
a message is printed.
