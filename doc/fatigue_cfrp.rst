==================
CFRP Fatigue Model
==================

Method
======

The fatigue calculation for fibre reinforced plastics (FRP), the method of using haigh diargams is
implemented. It is based on the work of Schokrih and Lessard :cite:`shokrieh2000progressive` and Lüders
:cite:`luders2020mehrskalige` (especially chapter 8.1.2).

It describes a fatigue stress factor f, that is dependent on the normalized amplitude and normalized
mean stress.

.. math::
    f = \frac{normAmplitude}{(1 - normMeanstress)^u \cdot (c + normMeanstress)^v}

.. math::
    normAmplitude = \frac{(\sigma_{Max} - \sigma_{Min}} {2 \cdot tensileStrength}

.. math::
    normMeanstress = \frac{(\sigma_{Max} + \sigma_{Min}} {2 \cdot tensileStrength}

Here u, v are fitted parameters and c is the strength ratio (compressionStrength / tensileStrength).
This fatigue stress factor f can be applied in the master curve

.. math::
    f = C\cdot N_f^D

to obtain the number of cycles till failure.

|parameters|

.. mermaid::

    flowchart TB
        a[define cycles & occurences]
        b[obtain strength, fatigue properties]
        c[calculate stresses for each load level]
        d["calculate cycles to failure
        for each layer, element, stress direction"]
        e["perform linear damage accumulation
        for each layer, element, stress direction"]
        f[return maximum fatigue damage]
        a --> b
        b --> c
        c --> d
        d --> e
        e --> f

Results
=======

Using the configuration **design hytazer_smr_iff_2.0bar.yaml**.
It utilizes a tank with a diameter of approx 3.5m and material properties
obtained at room temperature. With::

    useHydrostaticPressure: false
    valveReleaseFactor: 1.0
    cycles: 50000
    heatUpCycles: 1000
    simulatedTankLives: 5

and the variations of **safetyFactor** and **failureMode**, the following results were obtained:

+---------------------+---------------+---------------------------+-------------------+---------------+----------------------+
|                     | safety factor | mass static strength only | mass fatigue only | puck strength | fatigue damage level |
+=====================+===============+===========================+===================+===============+======================+
| fibre failure       | 1.8           | 28                        | 28                | 0.98          | 1e-17                |
+---------------------+---------------+---------------------------+-------------------+---------------+----------------------+
| fibre failure       | 1.5           | 28                        | 28                | 0.82          | 1e-17                |
+---------------------+---------------+---------------------------+-------------------+---------------+----------------------+
| inter fibre failure | 1.8           | 133                       | 121               | 0.95          | 1e-05                |
+---------------------+---------------+---------------------------+-------------------+---------------+----------------------+
| inter fibre failure | 1.5           | 121                       | 121               | 0.90          | 2e-05                |
+---------------------+---------------+---------------------------+-------------------+---------------+----------------------+
| inter fibre failure | 1.3           | 117                       | 121               | 0.78          | 3e-05                |
+---------------------+---------------+---------------------------+-------------------+---------------+----------------------+

For fibre failure, it can be seen that static strength at ultimate load is by several
orders of magnitude more important than
the fatigue damage criterion at limit load.
For inter fibre failure, fatigue damage in 22-direction is the most relevant case of all fatigue directions.
Still, only for a safety factor of 1.3, the fatigue criterion is the sizing one compared to
static strength, which can be seen by the mass comparison. With safety factor of 1.5 and above,
static strength is more important for sizing.

Remark: fatigue damage level is reduced exponentially by adding one additional layer compared
to the more linear behavior of the static strength criterion.




.. |parameters| image:: images/fatigue_cfrp/parameters.png
  :height: 300
