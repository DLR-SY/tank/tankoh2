Optimization Options
=====================
This sections describes some of the options that can be used to tailor the optimization of composite tanks.


hoopShiftRange
-----------------------------------------------------
By default, the hoop shifts of all hoop layers are distributed linearly between a maximum and minimum
value,which are defined by +relRadiusHoopLayerEnd. If hoopShiftPattern is set to an integer value, the
optimizer will always distribute that many hoop layer shifts between the maximum and minimum, repeating
the pattern as more hoop layers are added. This option can decrease the stress peaks in the transitional
dome region and lead to better designs.


hoopLayerCluster
-----------------------------------------------------
If hoopLayerCluster is set to an integer value, the optimizer will always create that
many hoop layers in a row. This speeds up the optimization progress, because the helical vs.
hoop evaluation is performed less often, and matches more closely to real winding layups,
where constantly switching between hoop and helical layers needs many transitional layers as
the winding angle is changed. If the failure criterion in the hoop layers is satisfied, the
optimizer will stop adding hoop layers and revert to the normal optimization. The following
image shows the transition region of a tank optimized with hoopShiftPattern = 8 and
hoopLayerCluster = 8.

|hoopLayerCluster|

.. |hoopLayerCluster| image:: images/optimization/hoopLayerCluster.png
	:width: 45 %


sortLayers
-----------------------------------------------------
The option sortLayers can be beneficial for tank designs. After each new
layer is added, the helical layers are sorted by rising winding angle.
This design prevents layers being wound over "bumps" in the dome and should
lead to lower bending stresses in the dome region.

|unsorted| |sorted|

.. |unsorted| image:: images/optimization/unsorted.png
	:width: 45 %
.. |sorted| image:: images/optimization/sorted.png
	:width: 45 %

sortLayersAboveAngle
-----------------------------------------------------
This is an additional option for sortLayers. For thicker tanks, it may not be beneficial to
sort all layers, because it becomes impossible to manage the thickness buildup in the polar
region. Setting this angle makes it so that only higher angles are sorted towards the outside.


enforceWindableContour
-----------------------------------------------------

Setting the option enforceWindableContour to true adds a punishment value to the target function if the dome contour
does not match windability criteria. Currently this means that the first derivative of the thickness
can not be greater than the value set in settings.py and also can not be negative in the fitting
region (which would indicate concavities.
