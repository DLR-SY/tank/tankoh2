<!--
SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)

SPDX-License-Identifier: MIT
-->

# Changelog

Versions follow [SemVer](https://semver.org/) with a strict backwards compatibility policy.
The third digit is only for regressions.

## [2.5.0] - Unreleased
### Added

## [2.4.1] - 11.11.2022
### Modified
- bugfix for width/height parameter input

## [2.4.0] - 11.11.2022
### Added
- handle varying roving width/thickness between hoop and helical layers #64


## [2.3.2] - 31.08.2022
### Added

- improve target function #60, #62. Now a weighted sum of variations of puck and mass can be used

## [2.3.1] - 31.08.2022
### Added

- unsymmetric tanks finalized

## [2.3.0] - 20.07.2022
### Added

- isotensoid dome
- conical dome geometry finalized
- bug fix in optimizer
- issue 50 include liner thickness to volume calculation

### Changed

## [2.2.0] - 24.06.2022
### Finalized
- issue 1 puck iff is also working properly
- issue 44 included stochastic seed for optimizer in settings file
- issue 43 included options for predefined angles
- issue 42 added plot for target functions
- issue 41 removed verbose argument and use log.debug instead
- issue 34 included use of coolprop
- issue 27 output rst-able tables
- issue 31 added matal fatigue methods
- issue 36 fitting geometry is defined based on PO radius and cylinder radius

### Started
- issue 40
- issue 12 dome contour - conical domes are implemented but still work in progress


## [0.0.0] Example
### Added
- foo

### Changed
- foo

### Removed
- foo

### Fixed
- foo
